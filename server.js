const express = require("express")
const compression = require("compression")
const next = require("next")

const dev = process.env.NODE_ENV !== "production"
const app = next({ dev })
const handle = app.getRequestHandler()

app.prepare().then(() => {
  const server = express()
  server.use(compression())

  server.use(handle)

  server.listen(process.env.PORT || 3000, err => {
    if (err) throw err
    console.log("> Ready on http://localhost:3000")
  })
})

const prod = process.env.NODE_ENV === "production"

module.exports = {
  presets: ["next/babel", "@zeit/next-typescript/babel"],
  plugins: [
    [
      "transform-define",
      { "process.env.BACKEND_URL": prod ? process.env.SERVER_URL : "http://localhost:8000" }
    ]
  ]
}

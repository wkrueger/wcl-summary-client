import ReactRedux = require("react-redux")
import Redux = require("redux")
import { ThunkDispatcher, Redutser } from "redutser"

declare global {
  interface Window {
    devToolsExtension: any
    $SERVER_URL: string
  }

  namespace T {
    export type FirstArg<T> = T extends (i: infer V) => any ? V : never

    export type StoreAction<Store> = Store extends Redux.Store<any, infer Action> ? Action : never
    export type StoreState<Store> = Store extends Redux.Store<infer State, any> ? State : never

    /*
    export interface ChangeProps<PropsToRemove, PropsToAdd> {
      <P extends Shared<PropsToRemove, P>>(component: Component<P>): ComponentClass<
        Omit<P, keyof Shared<PropsToRemove, P>> & PropsToAdd
      > //& { WrappedComponent: Component<P> }
    }

    export type RemoveProps<PropsToRemove> = ChangeProps<PropsToRemove, {}>
    */

    type ChangeComponentProps<Comp, ToRemove = {}, ToAdd = {}> = Comp extends React.SFC<infer Props>
      ? React.SFC<Omit<Props, keyof ToRemove> & ToAdd>
      : never

    type ComponentEnhancer<ToRemove, ToAdd> = <Comp extends React.SFC<ToRemove & ToAdd>>(
      comp: Comp
    ) => ChangeComponentProps<Comp, ToRemove, ToAdd>

    export interface ConnectFromStore {
      <
        Store,
        StateProps = StoreState<Store>,
        DispatchProps = { dispatch: ThunkDispatcher<StoreAction<Store>, StoreState<Store>> },
        OwnProps = {}
      >(
        stateMapper: (state: StoreState<Store>) => StateProps,
        dispatchMapper: (
          dispatcher: ThunkDispatcher<StoreAction<Store>, StoreState<Store>>
        ) => DispatchProps
      ): ComponentEnhancer<StateProps & DispatchProps & OwnProps, OwnProps>
    }
  }

  export interface ServiceReturn {
    reportId: string
    deathCut: number
    result: any
    entities: any
  }
}

/*
    <TStateProps = {}, TDispatchProps = {}, TOwnProps = {}, State = {}>(
        mapStateToProps: MapStateToPropsParam<TStateProps, TOwnProps, State>,
        mapDispatchToProps: MapDispatchToPropsParam<TDispatchProps, TOwnProps>
    ): InferableComponentEnhancerWithProps<TStateProps & TDispatchProps & TOwnProps, TOwnProps>;
*/

const withCSS = require("@zeit/next-css")
const withTypescript = require("@zeit/next-typescript")
const ForkTsCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin")

module.exports = withCSS(
  withTypescript({
    webpack(config, options) {
      // Do not run type checking twice:
      if (options.isServer && process.env.NODE_ENV !== "production")
        config.plugins.push(new ForkTsCheckerWebpackPlugin())

      return config
    }
  })
)

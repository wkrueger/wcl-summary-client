import { createRedutser, ThunkDispatcher } from "redutser"
import { Middleware } from "redux"
import { createStore } from "redux"
import { compose } from "redux"
import { applyMiddleware } from "redux"
import axios from "axios"
import thunkMw from "redux-thunk"
import { get as _get, groupBy as _groupBy, values as _values } from "lodash"
import { denormalize } from "normalizr"
import * as Schemas from "./useless/normalize-schemas"
import { Set } from "immutable"

const initialState = {
  report: {} as {
    waitingForIt?: string | undefined
    content?: {
      /*raw: ServiceReturn;*/
      supportedFightIds: number[]
      deathCut: number
      reportInfo: any
      deathsFull: any
      deathsByPlayer: any
      deathsByAbility: any
      interaction: {
        expandAbilities: Set<string>
      }
    }
  },
  error: undefined as Error | undefined
}

export const redutser = createRedutser(initialState, {
  waitingForReport: (state, action: { reportId: string }) => ({
    ...state,
    error: undefined,
    report: { waitingForIt: action.reportId }
  }),

  displayReport: (state, a: { raw: ServiceReturn }) => {
    const deathsFull = denormalize(
      Object.keys(a.raw.entities.deathEvents),
      [Schemas.deathEvent],
      a.raw.entities
    )
    const out = {
      ...state,
      error: undefined,
      report: {
        waitingForIt: undefined,
        content: {
          //raw: a.reportContent,
          deathCut: a.raw.deathCut,
          reportInfo: denormalize(
            _values(a.raw.entities.reports)[0],
            Schemas.report,
            a.raw.entities
          ),
          supportedFightIds: _values(a.raw.entities.mainResponse)[0].supportedFights,
          deathsFull,
          deathsByPlayer: _groupBy(deathsFull, "source.name"),
          deathsByAbility: _groupBy(deathsFull, "killingBlow.name"),
          interaction: {
            expandAbilities: Set<string>()
          }
        }
      }
    }
    console.log("reportstate", out.report.content)
    return out
  },

  expandAbility: (state, a: { which: string }) => ({
    ...state,
    report: {
      waitingForIt: undefined,
      content: state.report.content && {
        ...state.report.content,
        interaction: {
          expandAbilities: !state.report.content.interaction.expandAbilities.contains(a.which)
            ? state.report.content.interaction.expandAbilities.add(a.which)
            : state.report.content.interaction.expandAbilities.remove(a.which)
        }
      }
    }
  }),

  error: (_, a: { error: Error }) => ({
    error: a.error,
    report: {}
  })
})
const actions = redutser.creators

export const effects = {
  fetchReport: (a: { reportId: string; deathCut: number }) => async (dispatch: Dispatcher) => {
    const regexd = regexReport(a.reportId)
    dispatch(actions.waitingForReport({ reportId: regexd }))
    let resp: ServiceReturn = (await http.get("report", {
      params: { reportId: regexd, deathCut: a.deathCut || 2 }
    })).data
    console.log("report", resp)
    dispatch(actions.displayReport({ raw: resp }))
  }
}

function regexReport(repAddr: string) {
  const matched = repAddr.replace(/.*\/reports\/(\w+)([#|\/]*)(.*)/g, "$1")
  if (!matched.length) return repAddr
  return matched
}

// --------------

function formatError(err: any): any {
  let axiosError: any
  if ((axiosError = _get(err, "response.data.error"))) {
    return formatError(axiosError)
  }
  if (err.message) {
    return { error: { message: err.message, type: err.type, stack: err.stack } }
  }
  if (err.error) {
    return formatError(err.error)
  }
  return { error: { message: String(err) } }
}

const asyncThunkErrorMw: Middleware = api => dispatchNext => action => {
  if (typeof action === "function") {
    let actionNext = (...i: any[]) => {
      try {
        var ret = action(...i)
      } catch (syncErr) {
        console.error(syncErr)
        api.dispatch({ type: "error", payload: formatError(syncErr) })
      }
      if (ret.then) {
        ret.catch((asyncErr: any) => {
          console.error(asyncErr)
          api.dispatch({ type: "error", payload: formatError(asyncErr) })
        })
      }
      return ret
    }
    dispatchNext(actionNext)
  } else {
    dispatchNext(action)
  }
}

export const store = createStore(
  redutser.reducer,
  compose<any>(
    applyMiddleware(asyncThunkErrorMw, thunkMw),
    typeof window !== "undefined" && window.devToolsExtension
      ? window.devToolsExtension()
      : (f: any) => f
  )
)

console.log("backend url", process.env.BACKEND_URL)

const http = axios.create({
  baseURL: process.env.BACKEND_URL,
  headers: {
    Accept: "application/json"
  }
})

type Dispatcher = ThunkDispatcher<T.StoreAction<typeof store>, T.StoreState<typeof store>>

import { connect } from "react-redux"
import { ThunkDispatcher, Redutser } from "redutser"

export const connext: ConnectFromRedutser = connect as any

export type ConnectFromRedutser = <
  Red extends Redutser<any, any>,
  StateProps = Red["initialState"],
  DispatchProps = { dispatch: ThunkDispatcher<Red["actionTypes"], Red["initialState"]> },
  OwnProps = {}
>(
  stateMapper: (state: Red["initialState"]) => StateProps,
  dispatchMapper: (
    dispatcher: ThunkDispatcher<Red["actionTypes"], Red["initialState"]>
  ) => DispatchProps
) => T.ComponentEnhancer<StateProps & DispatchProps & OwnProps, OwnProps>

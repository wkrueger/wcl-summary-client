import React from "react"

import { redutser } from "./reducers"
import { connext } from "./useless/connext"
import { toPairs as _pairs, countBy as _countBy } from "lodash"
import "./deaths-by.css"
//import { FightEventModels } from '../../Wipefest.Core'

export const DeathsByAbility = connext<typeof redutser>(state => state, dispatch => ({ dispatch }))(
  p => {
    let deathsMap = _pairs<any[]>(p.report!.content!.deathsByAbility).sort(
      (a, b) => b[1].length - a[1].length
    )
    let abilitiesToExpand = p.report!.content!.interaction.expandAbilities
    return (
      <div>
        <table className="ui table deathlist">
          <thead />
          <tbody>
            {deathsMap.map(([abilityName, events]) => {
              return (
                <React.Fragment key={abilityName}>
                  <tr
                    className="header-row"
                    onClick={() =>
                      p.dispatch(redutser.creators.expandAbility({ which: abilityName }))
                    }
                  >
                    <td>{abilityName}</td>
                    <td>{events.length}</td>
                  </tr>
                  {abilitiesToExpand.contains(abilityName) && (
                    <tr>
                      <td colSpan={2}>
                        <InnerTable deathsByAbility={events} />
                      </td>
                    </tr>
                  )}
                </React.Fragment>
              )
            })}
          </tbody>
        </table>
      </div>
    )
  }
)

const InnerTable: React.SFC<{ deathsByAbility: any[] }> = p => {
  const count = _pairs(_countBy(p.deathsByAbility, "source.name")).sort((a, b) => b[1] - a[1])
  return (
    <table className="ui table inner">
      <thead>
        <tr>
          <th>Player</th>
          <th>Deaths</th>
        </tr>
      </thead>
      <tbody>
        {count.map(([k, v]) => (
          <tr key={k}>
            <td>{k}</td>
            <td>{v}</td>
          </tr>
        ))}
      </tbody>
    </table>
  )
}

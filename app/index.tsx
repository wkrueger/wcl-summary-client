import { Container, Segment, Header, Form, Message, Tab, Grid, Loader } from "semantic-ui-react"
import { Provider } from "react-redux"
import { store, effects, redutser } from "./reducers"
import { connext } from "./useless/connext"
import { DeathsByAbility } from "../app/deaths-by-ability"
import "./index.css"

const ConnectHoc = connext<typeof redutser>(state => state, dispatch => ({ dispatch }))

const Content: T.FirstArg<typeof ConnectHoc> = p => (
  <Container>
    <Segment vertical>
      <Header as="h1">Report summaries</Header>
    </Segment>
    <Segment vertical>
      {p.error && <Message error>{p.error.message}</Message>}
      <Form
        onSubmit={ev => {
          const form: any = ev.target
          const reportId: string = form.reportId.value
          const deathCut = Number(form.deathCut.value || 0)
          p.dispatch(effects.fetchReport({ reportId, deathCut }))
        }}
      >
        <Form.Group>
          <Form.Input placeholder="Report link" name="reportId" width={12} />
          <Form.Input placeholder="Death cut = 2" name="deathCut" width={4} />
        </Form.Group>
        <Form.Group>
          <Form.Button content="Submit" type="submit" />
        </Form.Group>
      </Form>
    </Segment>
    {p.report.waitingForIt && (
      <Segment vertical>
        <Container text>
          <Loader active inline size="tiny" />
          Waiting for {p.report.waitingForIt}
        </Container>
      </Segment>
    )}
    {p.report.content && (
      <Segment vertical>
        <Grid columns={4}>
          <Grid.Row>
            <Grid.Column width={4}>
              <h2>Report</h2>
            </Grid.Column>
            <Grid.Column width={12}>
              <table className="ui table report-detail">
                <tbody>
                  <tr>
                    <td>Name</td>
                    <td>{p.report!.content!.reportInfo.title}</td>
                  </tr>
                  <tr>
                    <td>Start</td>
                    <td>{_dateFormat(p.report!.content!.reportInfo.start)}</td>
                  </tr>
                  <tr>
                    <td>Fights</td>
                    <td>{p.report!.content!.supportedFightIds.length}</td>
                  </tr>
                  <tr>
                    <td>Cut after deaths</td>
                    <td>{p.report!.content!.deathCut}</td>
                  </tr>
                </tbody>
              </table>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Segment>
    )}
    <Segment vertical>
      {p.report &&
        p.report.content && (
          <Tab
            panes={[
              { menuItem: "Deaths by ability", render: () => <DeathsByAbility /> },
              { menuItem: "Deaths by player", render: () => <div /> }
            ]}
          />
        )}
    </Segment>
  </Container>
)

function _dateFormat(n: number) {
  let d = new Date(n)
  return `${d.toLocaleString()}`
}

const Merged = ConnectHoc(Content)

export default () => (
  <Provider store={store as any}>
    <Merged />
  </Provider>
)
